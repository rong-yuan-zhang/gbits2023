using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SweepTime : MonoBehaviour
{
    public GameObject sweepBar;
    public Litter litter;
    public Canvas canvas;
    [SerializeField] Vector2 UIPos = new Vector2();
        GameObject sB;
    private void Start()
    {
       sB=Instantiate(sweepBar,canvas.transform);
    }
    // Update is called once per frame
    void Update()
    {
       // p = litter.isSweep;
        if(litter.isSweep)
        {
            sB.GetComponent<Image>().enabled = true;
            Vector2 originalPos = new Vector2(litter.thisPosX, litter.thisPosY);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, originalPos, null, out UIPos);
            sB.transform.position = new Vector2(UIPos.x + 960, UIPos.y + 540);
            sB.GetComponent<Image>().fillAmount = 1 - litter._timer2 / litter.sweepCost;
        }
        else if(!litter.isSweep)
            sB.GetComponent<Image>().enabled = false;
    }
}
