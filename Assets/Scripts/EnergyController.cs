using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EnergyController : MonoBehaviour
{
    public LifeBar lifeBar;
    public Litter litter;
    public RectTransform Energy;
    public RectTransform EnergyLim;
    public RectTransform Frame;
    float OriginalWidth;
    public int sweepOfEnergy = -6;
    public int sweepOfEnergyLim = 3;
    public int careOfEnergy = -10;
    public int toyOfEnergy = -5;
    [SerializeField]
    int _timer = 0;//��ʱ��
    //float energyLimOriginalWidth;
    float width;
    float widthTransformed;
    float ratioToWidth;//ÿһ�ݶ�Ӧ�ĳ���

    public ObjectList objectList;
    Color orange = new Color(0.992f, 0.835f, 0.106f, 1);
    Color green = new Color(0.5f,1f,0.5f,1);
    // Start is called before the first frame update
    public void initial()
    {
        EnergyInterface.energyRatio = 100;
        EnergyInterface.energyLimRatio = 100;
        EnergyInterface.isSitting = false;
        EnergyInterface.isUnder10 = false;

        Frame.gameObject.SetActive(false);

        change(Energy, -70);
        change(EnergyLim, -70);//Initial��ʼΪ30
    }
    void Start()
    {           

        Energy = Energy.GetComponent<RectTransform>();
        EnergyLim = EnergyLim.GetComponent<RectTransform>();
        Frame = Frame.GetComponent<RectTransform>();
        OriginalWidth = Energy.rect.width;
        OriginalWidth = EnergyLim.rect.width;
        EnergyInterface.setOW(OriginalWidth);

        ratioToWidth = OriginalWidth / 100;
        EnergyInterface.setRTW(ratioToWidth);

        initial();

    }
    public void change(RectTransform Bar,int delta)
    {          
        width = Bar.rect.width;
        if (Bar == Energy)
        {
            if(EnergyInterface.energyRatio+delta> EnergyInterface.energyLimRatio)
            {
                delta = EnergyInterface.energyLimRatio - EnergyInterface.energyRatio;
            }
            if (EnergyInterface.energyRatio + delta <= 0)
            {
                lifeBar.deduct("energy");//���������׿۷�
                delta = -EnergyInterface.energyRatio;
            }
            EnergyInterface.energyChange(delta);
            //width = width+ratioToWidth*delta;
            widthTransformed = OriginalWidth * logRatio(EnergyInterface.energyRatio);
            Bar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, widthTransformed);
            Bar.transform.Translate(new Vector3((widthTransformed-width) /2, 0, 0));
            
        }
        else if (Bar == EnergyLim)
        {
            EnergyInterface.energyLimitChange(delta);
            //width = width + ratioToWidth * delta;
            widthTransformed = OriginalWidth * logRatio(EnergyInterface.energyLimRatio);
            Bar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, widthTransformed);
            Bar.transform.Translate(new Vector3((widthTransformed - width) / 2, 0, 0));
           
        }      
        //Debug.Log(EnergyInterface.energyRatio + " " + EnergyInterface.energyLimRatio);       
    }
    void showFrame()
    {
        Frame.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, OriginalWidth * logRatio(EnergyInterface.energyLimRatio));
        Frame.transform.Translate(new Vector3((EnergyLim.transform.position.x - Frame.transform.position.x), 0, 0));
        Frame.gameObject.SetActive(true);
    }
  
    void Update()
    {

        if (EnergyInterface.energyRatio < EnergyInterface.energyLimRatio )
        {
            _timer = _timer+(int)(Time.deltaTime*1000f);
            if(_timer >= 1000 && EnergyInterface.isSitting == false)
            {
                change(Energy, 1);
                _timer = 0;
            }
            if (_timer >= 1000 && EnergyInterface.isSitting == true)
            {
                change(Energy, 10);
                _timer = 0;
            }
        }//ÿ���1
        else
            _timer = 0;



        if (EnergyInterface.energyRatio != 0)
        {
            if (litter.endSweep)//����ɨ��Ϊtrue
            {
                change(Energy, sweepOfEnergy);
                change(EnergyLim, sweepOfEnergyLim);
                _timer = 0;
            }
            if (objectList.endCare)//����ɨ��Ϊtrue
            {
                change(Energy, careOfEnergy);
                _timer = 0;
                objectList.endCare = false;
            }
            if (objectList.endUseToy)//����ɨ��Ϊtrue
            {
                change(Energy, toyOfEnergy);
                _timer = 0;
                objectList.endUseToy = false;
            }       
        }
        if (EnergyInterface.isSitting)
        {
            showFrame();
        }
        else
        {
            Frame.gameObject.SetActive(false);
        }
        if (EnergyInterface.energyRatio <= EnergyInterface.energyLimRatio*0.21) { Energy.gameObject.GetComponent<Image>().color = Color.red; }
        else if (EnergyInterface.energyRatio > EnergyInterface.energyLimRatio * 0.21 && EnergyInterface.energyRatio <= EnergyInterface.energyLimRatio * 0.32) { Energy.gameObject.GetComponent<Image>().color = orange; }
        else { Energy.gameObject.GetComponent<Image>().color = green; }
    }


    float logRatio(float ratio)
    {
        return (float)Math.Log(ratio+1,101);       
    }
}
