using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePosition : MonoBehaviour
{
    public viewMove vm;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RayInterface.setRay(Camera.main.ScreenPointToRay(Input.mousePosition));//定义主摄像机射线(射向鼠标位置)
            RayInterface.isHit = Physics.Raycast(RayInterface.ray, out RayInterface.HitPointInfo);
            vm.viewmove();
        }
    }
}
