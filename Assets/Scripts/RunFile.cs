using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunFile : MonoBehaviour
{
    public GameObject bgm;
    private AudioSource bgmSource;
    // Start is called before the first frame update
    void Awake()
    {
        bgmSource=bgm.GetComponent<AudioSource>();
        
        bgmSource.Play();
        Screen.SetResolution(1920, 1080, false);


        AudioListener.pause  = OverallAudio.pause;
    }

   
}
