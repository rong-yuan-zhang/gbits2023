using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BabyCry : MonoBehaviour
{
    public LifeBar lifeBar;
    public float dampIndex = 1;
    float fenmu=1f;// 1/1.5 1/2 1/2.5 1/3

    public RedBubUI redBubUI;
    
    bool becared = false;
    [HideInInspector] public bool isHold = false;
    public bool isCry = false;
    public GameObject[] cryObjects = new GameObject[2];
    private AudioSource[] audioSource = new AudioSource[2];
    private AudioClip[] clip = new AudioClip[2];

    public float inintialTimes = 1f;//2倍太长了，先不动
    //private bool stopping = false;
    int cryTime = 1;
    public Chair chair;
    public ObjectUI objectUI;
    public ObjectList objectList;
    public GameObject @baby;

    
    public float[] delttime = { 8.00f, 2.00f, 6.00f, 3.00f, 4.00f, 6.00f };
    public float TimeTimes = 1.2f;//时间间隔，倍率，用于调整合适的值
    float TimeTimesDiff;//TimeTimes与1的差
    int i = 0;
    [SerializeField]
    public int _timer = 0;
    float fill;
    private float[] playTime = new float[2];
    private int cryIndex = 0;

    public GameObject text;
    private TextMeshProUGUI scoreText;

    public void reCry()
    {
        cryTime = 1;
        becared = false;
        i = 0;
    }
 
    private void Start()
    {
        scoreText = text.GetComponent<TextMeshProUGUI>();
        reCry();
        audioSource[0] = cryObjects[0].GetComponent<AudioSource>();
        audioSource[1] = cryObjects[1].GetComponent<AudioSource>();
      
        clip[0]= audioSource[0].clip;
        clip[1]= audioSource[1].clip;
       
        //时间间隔调整
        for (int j = 0; j < this.delttime.Length; j++)
        {
            this.delttime[j] = (float)this.delttime[j] * inintialTimes;
        }
       
        playTime[0] = clip[0].length;
        playTime[1] = clip[1].length;
        TimeTimesDiff = TimeTimes - 1;
    }
    
    void FixedUpdate()//Fixed比较准
    {
        //bubCtrl();
        if (isHold) { return; }//抱起后时间固定
        if (isCry)
        { scoreText.SetText("宝宝在哭TAT");}
        if (audioSource[0].isPlaying || audioSource[1].isPlaying) return;//哭的时候暂停

            if (_timer < delttime[i] * 1000)
             {
            //_timer += 2;//改成2应该差别很大
                if(cryTime==1)
                    _timer += (int)(Time.deltaTime * 1000f);
                else
                {
                    if (becared)
                        _timer += (int)(Time.deltaTime * 1000f);
                    if (!becared)
                        _timer += (int)(delttime[i] * 40f);//一直未照顾则0.5s秒哭一次  20/40
                }          
             }
        else if (_timer >= delttime[i] * 1000)
        {     
                cry();
                _timer = 0;
                       
            if (i == 5)
                i = 0;
            else
                i++;
        } 
    }

    
    void cry()
    {
        isCry = true;
        audioSource[cryIndex].PlayOneShot(clip[cryIndex]);
       
        becared = false;
        cryTime++;
        StartCoroutine(AfterPlayed(audioSource[cryIndex]));
    }
    IEnumerator AfterPlayed(AudioSource audioSource)
    {
        while (audioSource.isPlaying)
        {
            playTime[cryIndex] -= Time.deltaTime;
            yield return null;
        }
        if (playTime[cryIndex] < 0.1)//总之很小就对了，人类反应有限
        {
            //scoreText.SetText("抱起宝宝！");
            lifeBar.deduct("cry");
            bubChange();
            cryIndexChange(cryIndex);
        }
        playTime[cryIndex] = clip[cryIndex].length;
        yield break;
    }
    public void stopCry()
    {
        isCry = false;
        becared = true;
        isHold = true;
        if ((audioSource[cryIndex].isPlaying||redBubUI.redBub.GetComponent<Image>().sprite == redBubUI.Prepared[0])&&!redBubUI.isBling) {
            scoreText.SetText("做得好！");
            fill = 0;
            redBubUI.reBlue(); ;
            _timer = 0;
        }
        Invoke("settextvoid",1.5f);
        audioSource[cryIndex].Stop();
        //stopping = true;
    }
    public void putBaby()
    {
        isHold = false;
    }

    void bubChange()
    {
        fill += 0.21f;

        redBubUI.setFill(fill);
    
        if (fill >= 1)
        {
            fill = 0;
        }
    }

    void cryIndexChange(int index)
    {
        if(index == 0) { this.cryIndex = 1; }
        else if (index == 1) { this.cryIndex = 0; }
    }

    public void useToy()
    {
        
        for (int j = 0; j < this.delttime.Length; j++)
        {   
            this.delttime[j] = (float)this.delttime[j] * (1+ TimeTimesDiff * dampIndex);//指数衰减TimeTimesDiff*dampIndex
        }
        fenmu += 0.5f;
        dampIndex = 1/ fenmu;
    }
    void settextvoid(){
        scoreText.SetText("");
    }
}
