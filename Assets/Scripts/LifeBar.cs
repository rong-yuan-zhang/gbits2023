using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    public GameOver gameOver;
    public BabyCry babyCry;
    public GameObject[] lifeBar;
    // Start is called before the first frame update
    public int totalScore = 100;
    public int cryLoss = 10;    //婴儿哭完所扣分
    public int energyLoss = 50;  //精力条见底所扣分

    int cryScore = 0;
    //int cryHave;
    //int energyhave;

    public void deduct(string method)
    {
        if (method.StartsWith("c")){
            this.totalScore -= cryLoss;
            cryScore += cryLoss;
        }
        else if (method.StartsWith("e")){
            this.totalScore -= energyLoss;
            judge();
        }      
    }
    public void judge()
    {
        if (totalScore<=0)
        {
            lifeBarHave(0);
        }
        else if (totalScore>0&& totalScore<=50)
        {
            lifeBarHave(1);
            if (cryScore == 50)
            {
                babyCry.reCry();
            }
        }
        else if (totalScore > 50)
        {
            lifeBarHave(2);
        }
    }

    void lifeBarHave(int num)
    {
        switch (num)
        {
            case 0:
                {
                    lifeBar[0].GetComponent<Image>().enabled = false;
                    lifeBar[1].GetComponent<Image>().enabled = false;
                    gameOver.gameFaild();
                    break;
                }
            case 1:
                {
                    lifeBar[0].GetComponent<Image>().enabled = true;
                    lifeBar[1].GetComponent<Image>().enabled = false;
                    break;
                }
            case 2:
                {
                    lifeBar[0].GetComponent<Image>().enabled = true;
                    lifeBar[1].GetComponent<Image>().enabled = true;
                    break;
                }
        }          
    }
}
