using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartGame : MonoBehaviour
{
    public GameObject openDoor;
    private AudioSource audioSource = new AudioSource();
    private AudioClip clip ;
    private void Start()
    {
        audioSource= openDoor.GetComponent<AudioSource>();
        clip = audioSource.clip;
       // DontDestroyOnLoad(clip);
    }
    // Start is called before the first frame 
    public  void RunGame()
    {
        DontDestroyOnLoad(openDoor);
        audioSource.PlayOneShot(clip);
        SceneManager.LoadScene(1);
        
    }
}
