using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AudioControl : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite[] sp;
    public AudioSource bgm;
    private void Start()
    {
        this.gameObject.GetComponent<Image>().sprite = sp[Convert.ToInt32(OverallAudio.pause)];
    }
    public void audioControl()
    {
        
        if (bgm.isPlaying)
        {
            OverallAudio.pause = true;
            AudioListener.pause = true;
        }
        else
        {
            OverallAudio.pause = false;
            AudioListener.pause = false;
        }
        this.gameObject.GetComponent<Image>().sprite = sp[Convert.ToInt32(OverallAudio.pause)];
    }
}
