using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Introduce : MonoBehaviour
{
    // Start is called before the first frame 
    public GameObject[] otherButtons;
    public GameObject text;
    bool intr=false;
    public void intro()
    {
        intr = this.GetComponent<Image>().isActiveAndEnabled;
        intr = !intr;
        this.GetComponent<Image>().enabled = intr;
         if (intr)//指的是上一个状态
        {
            otherButtons[0].GetComponent<Image>().enabled = false;
            otherButtons[1].GetComponent<Image>().enabled = false;
            text.SetActive(true);
        }
        else 
        {
            otherButtons[0].GetComponent<Image>().enabled = true;
            otherButtons[1].GetComponent<Image>().enabled = true;
            text.SetActive(false);
        }
    }
}
   
