using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class viewMove : MonoBehaviour
{
    Camera cam;
    public Chair chair;
    public GameObject backGronnd;
    public GameObject Door_to_kitchen;
    public GameObject Door_to_living;
    public GameObject babybed;
    public GameObject bedface;
    public Litter litter;
    public GameObject ViewBackButton;
    public GameObject ChairButton;
    public GameObject kitchen;
    public GameObject livingroom;

    [SerializeField] int _timer1 = 0;
    [SerializeField] int level = 1;
    [SerializeField] public float timeForLevel1 = 40000f;//1000倍，实际为40s

    public AudioSource[] audioSource;
    private AudioClip[] clip;

    private float OriginalSize ;
    private Vector3 OriginalPosition = new Vector3();

    string nameDTK;
    string nameDTL;
    string nameBBB;
    float view_heigth;
    float view_width;
    float initialSize = 3.5f;
    Vector3 initialPosition ;
   
    void Start()
    {
        cam = Camera.main;
        
        ViewBackButton.GetComponent<Image>().enabled = false;
        ChairButton.GetComponent<Image>().enabled = true;

        Transform bg = backGronnd.GetComponent<Transform>();
        view_heigth = bg.localScale.y / 2;  //背景图的二分之一高度
        view_width = bg.localScale.x / 2;  //背景图的二分之一高度
        nameDTK = Door_to_kitchen.name;
        nameDTL = Door_to_living.name;
        nameBBB = babybed.name;
        this.transform.Translate(new Vector3(-view_width/3, -view_heigth/ 8, 0));
        initialPosition = this.transform.position;
        backData();
    }
       void Update()
         {
        if (_timer1 < timeForLevel1&& level==1)
        {
            _timer1 += (int)(Time.deltaTime * 1000f);
        }
        else if (_timer1 >= timeForLevel1 && level == 1)
        {
            level += 1;
            _timer1 = 0;
            
            StartCoroutine(viewUp());
            backData();
            litter.enterLevel2();
        }
         }
    IEnumerator viewUp()
    {
        while (initialSize < 4.5f && Vector3.Distance(initialPosition, new Vector3(0, 0, 0)) > 0.5)
        {
            //t += Time.deltaTime;
            initialSize = Mathf.Lerp(initialSize, 5f, Time.deltaTime * 0.7f);
            initialPosition = Vector3.Lerp(initialPosition, new Vector3(0,0,0), Time.deltaTime * 0.7f);
            cam.orthographicSize = initialSize;
            this.transform.position = initialPosition;
            yield return null;
        }
        cam.orthographicSize = 5;
        this.transform.position = new Vector3(0, 0, 0);
        yield break;
    }
    public void viewBack()//返回之前界面，
    {
        ChairButton.GetComponent<Image>().enabled = true;
        ViewBackButton.GetComponent<Image>().enabled = false;
        cam.orthographicSize = this.OriginalSize;

        this.transform.position = this.OriginalPosition;
        
        
    }
   public void viewmove()
    {
            if (RayInterface.isHit == true)
            {
                if (RayInterface.HitPointInfo.collider.name == nameDTK)//通过射线检测到的碰撞体名字判定
                {
                audioSource[0].PlayOneShot(audioSource[0].clip);
                StartCoroutine(AfterPlayed(audioSource[0], live_to_kitchen));
                
                RayInterface.isHit = false;

                // OtherOperationInterface.OtherOperation();//休息外的其他操作，会打断休息
                chair.interrupt();
            }
                if (RayInterface.HitPointInfo.collider.name == nameDTL)
                {
                audioSource[1].PlayOneShot(audioSource[1].clip);
                StartCoroutine(AfterPlayed(audioSource[1], kitchen_to_livingRoom));
                RayInterface.isHit = false;
                // OtherOperationInterface.OtherOperation();//休息外的其他操作，会打断休息
                chair.interrupt();

            }
                if (RayInterface.HitPointInfo.collider.name == nameBBB)//前往婴儿床
                {
                    backData();//保存之前的数据
                    to_babybed();
                    RayInterface.isHit = false;
                //OtherOperationInterface.OtherOperation();//休息外的其他操作，会打断休息
                     chair.interrupt();
                     ViewBackButton.GetComponent<Image>().enabled = true;
                     ChairButton.GetComponent<Image>().enabled = false;
            } 
        }
        
    }

    IEnumerator AfterPlayed(AudioSource audioSource,Action methd)
    {
        while (audioSource.isPlaying){
            yield return null;
        }
        methd();
        yield break;
    }
    //具体实现方法
    private void live_to_kitchen()
    {
        this.transform.position = kitchen.transform.position;
       //this.transform.Translate(new Vector3(0,view_heigth, 0));
    }
    private void kitchen_to_livingRoom()
    {
        this.transform.position = livingroom.transform.position;
        //this.transform.Translate(new Vector3(0,-view_heigth, 0));
    }
    private void to_babybed()
    {
        cam.orthographicSize = 5;
        this.transform.position = new Vector3(bedface.transform.position.x, bedface.transform.position.y, 0);
    }
    private void backData()
    {
        this.OriginalSize = cam.orthographicSize;
        this.OriginalPosition = this.transform.position;
    }
}
