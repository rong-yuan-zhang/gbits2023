using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface RayInterface
{
    static Ray ray;
    static RaycastHit HitPointInfo;
    static bool isHit = false;
    // Start is called before the first frame update
    public static void setRay(Ray r)
    {
        ray = r;
    }
}
