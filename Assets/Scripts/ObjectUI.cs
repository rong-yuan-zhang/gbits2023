using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ObjectUI : MonoBehaviour
{
    public Image image1;
    public Image image2;
    //public int[] q = new int[2];
    //public GameObject button;
    public ObjectList obLst;

    Sprite sprite0;
    Sprite sprite1;
    Sprite sprite2;
    Sprite sprite3;
    Sprite sprite4;

    public List<Sprite> spriteList = new List<Sprite>();
    //public Sprite sprite;
    // Update is called once per frame

    private void Start()
    {
        //Debug.Log("调用start");  //不会反复调用
        
        sprite0 = Resources.Load<Sprite>("Sprites/0") as Sprite;
        sprite1 = Resources.Load<Sprite>("Sprites/1") as Sprite;
        sprite2 = Resources.Load<Sprite>("Sprites/2") as Sprite;
        sprite3 = Resources.Load<Sprite>("Sprites/3") as Sprite;
        sprite4 = Resources.Load<Sprite>("Sprites/4") as Sprite;

        spriteList.Add(sprite0);
        spriteList.Add(sprite1);
        spriteList.Add(sprite2);
        //备用
        spriteList.Add(sprite3);
        spriteList.Add(sprite4);
    }
    
    public void SpriteChange()
    {
            image1.sprite = spriteList[obLst.objectList[0]];
            image2.sprite = spriteList[obLst.objectList[1]];

    }
    
}
