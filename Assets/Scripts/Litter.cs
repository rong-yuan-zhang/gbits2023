using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class Litter : MonoBehaviour
{
    public Chair chair;
    public GameObject prefabs;
    public ObjectList obLst;
    public int maxCount = 10;
    public float destroyTime = 1.5f;
    public int trashGenerate_time = 2000;
    public int trashGenerate_range = 1000;
    private int timetogenTrash;

    public float lx = -6.5f;
    public float rx = +1.5f;
    public float hy = -4f;
    public float ly = -4.5f;

    public AudioSource audioSource;
    private AudioClip clip;

    private int count = 0;
    [HideInInspector] public float thisPosX;
    [HideInInspector] public float thisPosY;
    [HideInInspector] public bool isSweep = false;
    [HideInInspector] public bool endSweep = false;
    private bool needProduce = true;
    [SerializeField]int _timer1 = 0;
    [SerializeField]public int _timer2 = 0;
    public float sweepCost = 1500f;
    public GameObject text;
    private TextMeshProUGUI scoreText;

    Ray ray;
    RaycastHit HitPointInfo;
    public void initial()
    {
        count = 0;
        isSweep = false;
        endSweep = false;
        needProduce = true;
        _timer1 = 0;
        _timer2 = 0;
        maxCount = 10;
    }
    private void Start()
    {
        initial();
        scoreText = text.GetComponent<TextMeshProUGUI>();

        timetogenTrash = 3000;

        clip = audioSource.clip;
    }
    void Update()
    {
        if(endSweep == true)
            endSweep = false;
        LitterProducer();
        if (Input.GetMouseButtonDown(0))
        {
            ray=Camera.main.ScreenPointToRay(Input.mousePosition);
            SweepDetect();           
        }
        sweep();
    }
    public void enterLevel2()
    {
      lx = -6f;
      rx = +6f;
      count = 0;
    }
    void LitterProducer()
    {
        if (count >= maxCount) return;
        if (!needProduce) return;
        if (_timer1 < timetogenTrash)//后设为4000
        {
            _timer1 += (int)(Time.deltaTime * 1000f);
        }
        else if (_timer1 >= timetogenTrash)
        {
            timetogenTrash = Random.Range(trashGenerate_time-trashGenerate_range,trashGenerate_time+trashGenerate_range); //更新新一次的随机范围
            Vector2 position = new Vector3(Random.Range(lx, rx), Random.Range(ly, hy));
            Quaternion rotatiao = Quaternion.Euler(0, 0, Random.Range(0,360));
            count++;
            _timer1 = 0;
            GameObject obj = Instantiate(prefabs, position, rotatiao);
            float sc = Random.Range(0.5f, 1.5f);
            obj.transform.localScale = new Vector3(sc,sc,0);
        }

    }
    void SweepDetect()
    {
        if (!Physics.Raycast(ray, out HitPointInfo)) return;
        if (HitPointInfo.collider.tag != "Litter") { return; }//通过射线检测到的碰撞体名字判定
        if (obLst.objectList[0] != (int)objectIndexs.Besom && obLst.objectList[1] != (int)objectIndexs.Besom)
        {
        scoreText.SetText("拿起扫把\n才能扫垃圾哦");
        Invoke("SetTextvoid",1.5f);
        return;
        }
        scoreText.SetText("干得好！");
        Invoke("SetTextvoid",1.5f);
        
        
        if (HitPointInfo.collider.gameObject.tag == "Litter")
        {
            Destroy(HitPointInfo.collider.gameObject);
            //Debug.Log(Input.mousePosition.x + " " + Input.mousePosition.y);
            Vector3 po = Input.mousePosition;
            thisPosX = (po.x <= Screen.width) ? po.x : Screen.width;
            thisPosY = (po.y <= Screen.height) ? po.y : Screen.height;
            isSweep = true;
        }
        //OtherOperationInterface.OtherOperation();//休息外的其他操作，会打断休息
        chair.interrupt();
        RayInterface.isHit = false;
        
    }
    void sweep()
    {
        if (!isSweep) return;
        if(!audioSource.isPlaying)
                 audioSource.PlayOneShot(clip);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        if (_timer2 < sweepCost)
        {
            _timer2 += (int)(Time.deltaTime * 1000f);
        }
        else if (_timer2 >= sweepCost)
        {
            _timer2 = 0;
            Cursor.lockState = CursorLockMode.None;
            Mouse.current.WarpCursorPosition(new Vector2(thisPosX, thisPosY));
            Cursor.visible = true;
            isSweep = false;
            endSweep = true;
        }
       
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var p1 = new Vector2(lx, ly);
        var p2 = new Vector2(lx, hy);
        var p3 = new Vector2(rx, hy);
        var p4 = new Vector2(rx, ly);
        //生成范围
        Gizmos.DrawLine(p1, p2);
        Gizmos.DrawLine(p2, p3);
        Gizmos.DrawLine(p3, p4);
        Gizmos.DrawLine(p4, p1);

    }
    void SetTextvoid(){
        scoreText.SetText("");
    }
}

