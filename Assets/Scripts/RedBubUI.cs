using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RedBubUI : MonoBehaviour
{
    public BabyCry babyCry;
    public LifeBar lifeBar;
    //Start is called before the first frame update
    public GameObject bub;
    public GameObject babyCot;
    public Canvas canvas;
    [SerializeField]Vector2 BubUIPos = new Vector2();
    [HideInInspector] public GameObject redBub ;
    Vector2 Pos;
    Vector2 screenPoint;

    float fill;
    [HideInInspector] public bool isBling=false;
    public int blingNum = 3;
    public float blinkSpeed = 0.6f;//闪烁速度
    //private bool isCutAlpha = true;//是否增加透明度
    [SerializeField] private float _timer3;//计时器
    public float timeval = 1f;//时间间隔
    public Sprite[] Prepared;
    Image img;
    public void initial()
    {
        blingNum = 3;
        setFill(0);
    }
    void Awake()
    {
        redBub = Instantiate(bub, canvas.transform);
        img = redBub.GetComponent<Image>();
    }
    private void Start()
    {
        initial();
        redBub.GetComponent<Image>().sprite = Prepared[1];
    }
    // Update is called once per frame
    void Update()
    {
        Pos = new Vector2(babyCot.transform.position.x, babyCot.transform.position.y + babyCot.transform.GetComponent<Collider>().bounds.size.y * 0.85f);
        screenPoint = Camera.main.WorldToScreenPoint(Pos);//世界转屏幕
        RectTransformUtility.ScreenPointToLocalPointInRectangle(this.canvas.transform as RectTransform, screenPoint, null, out BubUIPos);//屏幕转GUI
        redBub.transform.position = new Vector2(BubUIPos.x + 960, BubUIPos.y + 540);
        
        if (fill >= 1) {
            
            babyCry.reCry();
            bling(); 
        }
    }

    public void setFill(float fill)
    {
        redBub.GetComponent<Image>().sprite = Prepared[0];
        if (fill < 1)
            this.fill = fill;
        else if (fill >= 1)
        {
           
            this.fill = 1;
        }
        redBub.GetComponent<Image>().fillAmount =this. fill;
    }
    void bling()
    {
        isBling = true;
        if (blingNum > 0)
        {
            if (_timer3 < timeval)
            {
                redBub.GetComponent<Image>().color -= new Color(0, 0, 0, Time.deltaTime * blinkSpeed);
                _timer3 += Time.deltaTime;
            }
            else
            {
                redBub.GetComponent<Image>().color = new Color(img.color.r, img.color.g, img.color.b, 1);
                _timer3 = 0;
                blingNum--;
                
            }
        }
        else if (blingNum == 0)
        {
            reBlue();
            isBling = false;
        }
    }

    public void reBlue()
    {
        setFill(0);
        _timer3 = 0;
        blingNum = 3;
        lifeBar.judge();
        redBub.GetComponent<Image>().sprite = Prepared[1];
        redBub.GetComponent<Image>().fillAmount = 1;
    }
}
