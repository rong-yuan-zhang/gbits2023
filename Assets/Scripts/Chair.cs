using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chair : MonoBehaviour
{
    private RectTransform chair;
    float height;
    float width;
    // Start is called before the first frame update
    private void Start()
    {
        chair = this.GetComponent<RectTransform>();
        height = chair.rect.height;
        width = chair.rect.width;
    }
    public void interrupt()
    {
        EnergyInterface.isSitting = false;
        chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }
    public  void stateChange()
    {
        //Debug.Log("sss");
        if (!EnergyInterface.isSitting)//指的是上一个状态
        {
            chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1.5f * width);
            chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 1.5f * height);
        }
        else if (EnergyInterface.isSitting)
        {
            chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,  width);
            chair.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,  height);
        }
        EnergyInterface.isSitting = !EnergyInterface.isSitting;
    }
}
