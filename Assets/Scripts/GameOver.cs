using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using TMPro;
public class GameOver : MonoBehaviour
{
   
    public GameObject bgm;
    private AudioSource audioSource;
    private AudioClip clip;
    static int totalTime = 90000;
    int _timer = totalTime;
    int showTime;

    //public GameObject text;
    //private TextMeshProUGUI scoreText;
    private void Start()
    {
        //scoreText = text.GetComponent<TextMeshProUGUI>();
        _timer = totalTime;
        audioSource = bgm.GetComponent<AudioSource>();
        clip = audioSource.clip;
        // DontDestroyOnLoad(clip);
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (_timer > 0)
        {
            _timer -= (int)(Time.deltaTime * 1000f);
        }
        else if (_timer <= 0)
        {
            _timer = totalTime;
            gameFaild();
        }      
    }
    public void gameFaild()
    {
        
        Destroy(GameObject.Find("openDoor"));
        
        DontDestroyOnLoad(bgm);
        
        SceneManager.LoadScene(2);

    }
}
