using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//数据接口
interface EnergyInterface
{

    [HideInInspector] public static int energyRatio = 100;
    [HideInInspector] public static int energyLimRatio = 100; //实例子中会初始为30
    static float OriginalWidth;
    static float ratioToWidth;
    //建议先整数，设置长度时再除以100，避免float带来两位以后的的精度丢失 --get


    static bool isSitting = false;
    static bool isUnder10 = false;
    //static void refresh(float e, float eL)
    //{
    //    energyRatio = e;
    //    energyLimRatio = eL;
    //}
    public static void energyChange(int deltaEnergy)
    {
        energyRatio = energyRatio + deltaEnergy;
    }
    public static void energyLimitChange(int deltaEnergyLim)
    {
        energyLimRatio = energyLimRatio + deltaEnergyLim;
    }
    public static void setOW(float OW)
    {
        OriginalWidth = OW;
    }
    public static void setRTW(float RTW)
    {
        ratioToWidth = RTW;
    }
}
